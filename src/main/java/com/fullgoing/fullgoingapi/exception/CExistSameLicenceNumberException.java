package com.fullgoing.fullgoingapi.exception;

public class CExistSameLicenceNumberException extends RuntimeException {
    public CExistSameLicenceNumberException(String msg, Throwable t) {
        super(msg, t);
    }

    public CExistSameLicenceNumberException(String msg) {
        super(msg);
    }

    public CExistSameLicenceNumberException() {
        super();
    }
}
