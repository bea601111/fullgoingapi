package com.fullgoing.fullgoingapi.exception;

public class CWrongLicenceNumberException extends RuntimeException {
    public CWrongLicenceNumberException(String msg, Throwable t) {
        super(msg, t);
    }

    public CWrongLicenceNumberException(String msg) {
        super(msg);
    }

    public CWrongLicenceNumberException() {
        super();
    }
}
