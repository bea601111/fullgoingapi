package com.fullgoing.fullgoingapi.exception;

public class CIsLicenceFalseException extends RuntimeException {
    public CIsLicenceFalseException(String msg, Throwable t) {
        super(msg, t);
    }

    public CIsLicenceFalseException(String msg) {
        super(msg);
    }

    public CIsLicenceFalseException() {
        super();
    }
}
