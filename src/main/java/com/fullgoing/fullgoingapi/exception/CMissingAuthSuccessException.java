package com.fullgoing.fullgoingapi.exception;

public class CMissingAuthSuccessException extends RuntimeException {
    public CMissingAuthSuccessException(String msg, Throwable t) {
        super(msg, t);
    }

    public CMissingAuthSuccessException(String msg) {
        super(msg);
    }

    public CMissingAuthSuccessException() {
        super();
    }
}
