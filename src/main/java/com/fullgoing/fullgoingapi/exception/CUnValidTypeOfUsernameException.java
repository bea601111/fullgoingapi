package com.fullgoing.fullgoingapi.exception;

public class CUnValidTypeOfUsernameException extends RuntimeException {
    public CUnValidTypeOfUsernameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CUnValidTypeOfUsernameException(String msg) {
        super(msg);
    }

    public CUnValidTypeOfUsernameException() {
        super();
    }
}
