package com.fullgoing.fullgoingapi.exception;

public class CWrongAuthNumberException extends RuntimeException {
    public CWrongAuthNumberException(String msg, Throwable t) {
        super(msg, t);
    }

    public CWrongAuthNumberException(String msg) {
        super(msg);
    }

    public CWrongAuthNumberException() {
        super();
    }
}
