package com.fullgoing.fullgoingapi.exception;

public class CNoPermissionPutContentException extends RuntimeException {
    public CNoPermissionPutContentException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoPermissionPutContentException(String msg) {
        super(msg);
    }

    public CNoPermissionPutContentException() {
        super();
    }
}
