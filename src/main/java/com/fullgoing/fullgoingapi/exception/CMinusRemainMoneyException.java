package com.fullgoing.fullgoingapi.exception;

public class CMinusRemainMoneyException extends RuntimeException {
    public CMinusRemainMoneyException(String msg, Throwable t) {
        super(msg, t);
    }

    public CMinusRemainMoneyException(String msg) {
        super(msg);
    }

    public CMinusRemainMoneyException() {
        super();
    }
}
