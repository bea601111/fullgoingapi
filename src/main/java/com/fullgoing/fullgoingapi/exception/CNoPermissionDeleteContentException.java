package com.fullgoing.fullgoingapi.exception;

public class CNoPermissionDeleteContentException extends RuntimeException {
    public CNoPermissionDeleteContentException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoPermissionDeleteContentException(String msg) {
        super(msg);
    }

    public CNoPermissionDeleteContentException() {
        super();
    }
}
