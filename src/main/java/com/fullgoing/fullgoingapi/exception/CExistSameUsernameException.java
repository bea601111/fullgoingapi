package com.fullgoing.fullgoingapi.exception;

public class CExistSameUsernameException extends RuntimeException {
    public CExistSameUsernameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CExistSameUsernameException(String msg) {
        super(msg);
    }

    public CExistSameUsernameException() {
        super();
    }
}
