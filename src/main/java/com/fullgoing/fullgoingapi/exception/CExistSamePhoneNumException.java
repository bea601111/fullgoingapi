package com.fullgoing.fullgoingapi.exception;

public class CExistSamePhoneNumException extends RuntimeException {
    public CExistSamePhoneNumException(String msg, Throwable t) {
        super(msg, t);
    }

    public CExistSamePhoneNumException(String msg) {
        super(msg);
    }

    public CExistSamePhoneNumException() {
        super();
    }
}
