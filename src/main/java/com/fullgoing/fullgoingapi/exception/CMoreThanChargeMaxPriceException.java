package com.fullgoing.fullgoingapi.exception;

public class CMoreThanChargeMaxPriceException extends RuntimeException {
    public CMoreThanChargeMaxPriceException(String msg, Throwable t) {
        super(msg, t);
    }

    public CMoreThanChargeMaxPriceException(String msg) {
        super(msg);
    }

    public CMoreThanChargeMaxPriceException() {
        super();
    }
}
