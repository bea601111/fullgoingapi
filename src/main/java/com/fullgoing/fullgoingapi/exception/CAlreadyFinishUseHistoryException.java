package com.fullgoing.fullgoingapi.exception;

public class CAlreadyFinishUseHistoryException extends RuntimeException {
    public CAlreadyFinishUseHistoryException(String msg, Throwable t) {
        super(msg, t);
    }

    public CAlreadyFinishUseHistoryException(String msg) {
        super(msg);
    }

    public CAlreadyFinishUseHistoryException() {
        super();
    }
}
