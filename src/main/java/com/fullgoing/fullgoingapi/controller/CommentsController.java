package com.fullgoing.fullgoingapi.controller;

import com.fullgoing.fullgoingapi.entity.MessageBoard;
import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.model.comments.CommentsItem;
import com.fullgoing.fullgoingapi.model.comments.CommentsRequest;
import com.fullgoing.fullgoingapi.model.common.CommonResult;
import com.fullgoing.fullgoingapi.model.common.ListResult;
import com.fullgoing.fullgoingapi.service.messageBoard.MessageBoardService;
import com.fullgoing.fullgoingapi.service.comments.CommentsService;
import com.fullgoing.fullgoingapi.service.common.ResponseService;
import com.fullgoing.fullgoingapi.service.member.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "댓글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/comment")
public class CommentsController {
    private final CommentsService commentsService;
    private final MessageBoardService messageBoardService;

    @ApiOperation(value = "댓글 등록 /관리자")
    @PostMapping("/history-id/{boardHistoryId}")
    public CommonResult setComments(@PathVariable long boardHistoryId, @RequestBody @Valid CommentsRequest commentsRequest){
        MessageBoard messageBoard = messageBoardService.getBoardHistoryData(boardHistoryId);
        commentsService.setComments(messageBoard, commentsRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "댓글 리스트 /관리자")
    @GetMapping("/list/history-id/{historyId}")
    public ListResult<CommentsItem> getCommentsByPages(@PathVariable long historyId , @RequestParam(value = "page", required = false, defaultValue = "1") int page){
        MessageBoard messageBoard = messageBoardService.getBoardHistoryData(historyId);
        return ResponseService.getListResult(commentsService.getCommentsByPages(page, messageBoard), true);
    }

    @ApiOperation(value = "댓글 수정 /관리자")
    @PutMapping("/comment-id/{commentId}")
    public CommonResult putComment(@PathVariable long commentId, @RequestBody @Valid CommentsRequest commentsRequest){
        commentsService.putComment(commentId, commentsRequest);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "댓글 삭제 /관리자")
    @DeleteMapping("/comment-id/{commentId}")
    public CommonResult deleteComment(@PathVariable long commentId){
        commentsService.deleteComment(commentId);

        return ResponseService.getSuccessResult();
    }
}
