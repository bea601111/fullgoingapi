package com.fullgoing.fullgoingapi.controller;

import com.fullgoing.fullgoingapi.model.common.CommonResult;
import com.fullgoing.fullgoingapi.model.common.SingleResult;
import com.fullgoing.fullgoingapi.model.member.AuthCheckCodeResponse;
import com.fullgoing.fullgoingapi.model.member.MemberAuthCheckRequest;
import com.fullgoing.fullgoingapi.model.member.MemberAuthRequest;
import com.fullgoing.fullgoingapi.service.authCheck.AuthCheckService;
import com.fullgoing.fullgoingapi.service.common.ResponseService;
import com.fullgoing.fullgoingapi.service.member.MemberDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "인증 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/auth")

public class AuthCheckController {
    private final AuthCheckService authCheckService;
    private final MemberDataService memberDataService;

    @ApiOperation("인증 문자 보내기")
    @PostMapping("/message/send")
    public CommonResult setAuthCheck(@RequestBody @Valid MemberAuthRequest authRequest){
        memberDataService.phoneNumCheckForAuth(authRequest.getPhoneNumber());
        authCheckService.setAuthCheck(authRequest);
        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "인증번호 확인")
    @PutMapping("/number/check")
    public CommonResult checkAuthSuccess(@RequestBody @Valid MemberAuthCheckRequest memberAuthCheckRequest){
        authCheckService.authCheck(memberAuthCheckRequest.getPhoneNumber(), memberAuthCheckRequest.getAuthNum());

        return ResponseService.getSuccessResult();
    }

}
