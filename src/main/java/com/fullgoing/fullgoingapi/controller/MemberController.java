package com.fullgoing.fullgoingapi.controller;

import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.enums.MemberGroup;
import com.fullgoing.fullgoingapi.model.common.CommonResult;
import com.fullgoing.fullgoingapi.model.member.MemberCreateRequest;
import com.fullgoing.fullgoingapi.model.member.MemberJoinRequest;
import com.fullgoing.fullgoingapi.model.member.MemberLicenceNumUpdateRequest;
import com.fullgoing.fullgoingapi.model.member.MemberPasswordPhoneNumUpdateRequest;
import com.fullgoing.fullgoingapi.service.common.ResponseService;
import com.fullgoing.fullgoingapi.service.member.MemberDataService;
import com.fullgoing.fullgoingapi.service.member.ProfileService;
import com.fullgoing.fullgoingapi.service.remainingAmount.RemainingAmountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "회원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberDataService memberDataService;
    private final RemainingAmountService remainingAmountService;

    @ApiOperation(value = "회원 등록 /관리자")
    @PostMapping("/data")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest memberJoinRequest) {
        Member member = memberDataService.setAdmin(memberJoinRequest.getMemberGroup(), memberJoinRequest);
        remainingAmountService.setRemainingAmount(member);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 가입")
    @PostMapping("/join")
    public CommonResult setNormalMember(@RequestBody @Valid MemberCreateRequest memberCreateRequest) {
        Member member = memberDataService.setMember(MemberGroup.ROLE_USER, memberCreateRequest);
        remainingAmountService.setRemainingAmount(member);
        return ResponseService.getSuccessResult();
    }
}
