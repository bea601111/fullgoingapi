package com.fullgoing.fullgoingapi.controller;

import com.fullgoing.fullgoingapi.entity.KickBoard;
import com.fullgoing.fullgoingapi.entity.KickBoardHistory;
import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.entity.RemainingAmount;
import com.fullgoing.fullgoingapi.model.common.CommonResult;
import com.fullgoing.fullgoingapi.model.common.ListResult;
import com.fullgoing.fullgoingapi.model.common.SingleResult;
import com.fullgoing.fullgoingapi.model.kickBoardHistory.KickBoardHistoryOwnHistoryItem;
import com.fullgoing.fullgoingapi.model.kickBoardHistory.KickBoardHistoryOwnResponse;
import com.fullgoing.fullgoingapi.model.kickBoardHistory.AmountChargeHistorySaleStatsItem;
import com.fullgoing.fullgoingapi.model.kickBoardHistory.MyKickBoardUsingResponse;
import com.fullgoing.fullgoingapi.model.kickboard.KickBoardUseEndRequest;
import com.fullgoing.fullgoingapi.model.kickboard.KickBoardUseStartRequest;
import com.fullgoing.fullgoingapi.service.amountChargingHistory.AmountChargingHistoryService;
import com.fullgoing.fullgoingapi.service.common.ResponseService;
import com.fullgoing.fullgoingapi.service.kickBoard.KickBoardService;
import com.fullgoing.fullgoingapi.service.kickBoardHistory.KickBoardHistoryService;
import com.fullgoing.fullgoingapi.service.member.ProfileService;
import com.fullgoing.fullgoingapi.service.remainingAmount.RemainingAmountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "킥보드 사용 내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/kick-board-history")
public class KickBoardHistoryController {
    private final KickBoardHistoryService kickBoardHistoryService;
    private final RemainingAmountService remainingAmountService;
    private final AmountChargingHistoryService amountChargingHistoryService;
    private final KickBoardService kickBoardService;
    private final ProfileService profileService;

    @ApiOperation(value = "킥보드 사용 시작 /회원 및 관리자")
    @PostMapping("/start-use/kick-board-id/{kickBoardId}")
    public CommonResult setKickBoardHistoryStart(@PathVariable long kickBoardId, @RequestBody @Valid KickBoardUseStartRequest kickBoardUseStartRequest){
        Member member = profileService.getMemberData(); //멤버 데이터 토큰에서 가져오기
        KickBoard kickBoard = kickBoardService.getData(kickBoardId); // 사용할 킥보드 id 선택하기
        remainingAmountService.checkRemainMoney(member, kickBoard.getPriceBasis().getBasePrice()); //잔액이 사용할 킥보드 기본요금 미만이면 사용 불가
        kickBoardHistoryService.setKickBoardHistoryStart(member, kickBoard, kickBoardUseStartRequest); //킥보드 사용 시작

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "킥보드 사용 종료 /회원 및 관리자")
    @PutMapping("/end-use/history-id/{historyId}")
    public CommonResult putKickBoardHistoryEnd(@PathVariable long historyId, @RequestBody @Valid KickBoardUseEndRequest kickBoardUseEndRequest){
        Member member = profileService.getMemberData();
        RemainingAmount remainingAmount = remainingAmountService.getDataByMember(member);
        KickBoardHistory kickBoardHistory = kickBoardHistoryService.putKickBoardHistoryEnd(
                historyId,
                remainingAmount.getRemainPass(),
                kickBoardUseEndRequest);
        double mileage = remainingAmountService.putRemainPricePass(member, kickBoardHistory.getResultPrice(), kickBoardHistory.getResultPass());
        amountChargingHistoryService.plusMileage(member, mileage);


        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "자신의 이용 내역 보여주기 /회원 및 관리자")
    @GetMapping("/my")
    public ListResult<KickBoardHistoryOwnHistoryItem> getOwnUseHistory(@RequestParam(value = "page", required = false, defaultValue = "1") int page){
        Member member = profileService.getMemberData();
        return ResponseService.getListResult(kickBoardHistoryService.getOwnUseHistory(member,page), true);
    }

    @ApiOperation(value = "자신의 이용 내역 상세보기 /회원 및 관리자")
    @GetMapping("/my/history-id/{historyId}")
    public SingleResult<KickBoardHistoryOwnResponse> getOwnUseHistory(@PathVariable long historyId){
        Member member = profileService.getMemberData();
        return ResponseService.getSingleResult(kickBoardHistoryService.getOwnUseSingleHistory(member, historyId));
    }
    @ApiOperation(value = "자신의 사용 중인 킥보드 이용내역 상세보기 /회원 및 관리자")
    @GetMapping("/my/using/history")
    public SingleResult<MyKickBoardUsingResponse> getMyUsingHistory(){
        Member member = profileService.getMemberData();
        return ResponseService.getSingleResult(kickBoardHistoryService.getMyUsingHistory(member));
    }


}
