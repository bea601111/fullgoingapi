package com.fullgoing.fullgoingapi.controller;

import com.fullgoing.fullgoingapi.model.common.ListResult;
import com.fullgoing.fullgoingapi.model.kickBoardHistory.AmountChargeHistorySaleStatsItem;
import com.fullgoing.fullgoingapi.service.amountChargingHistory.AmountChargingHistoryService;
import com.fullgoing.fullgoingapi.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@Api(tags = "결제 내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/payment")
public class PaymentController {
    private final AmountChargingHistoryService amountChargingHistoryService;

    @ApiOperation(value = "매출 내역 보여주기 /관리자")
    @GetMapping("/histories")
    public ListResult<AmountChargeHistorySaleStatsItem> getSaleStats(
            @RequestParam(value = "start-day", required = false, defaultValue = "2023-01-01") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate start,
            @RequestParam(value = "end-day", required = false, defaultValue = "2023-12-31") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end,
            @RequestParam(value = "page", required = false, defaultValue = "1")int page){

        return ResponseService.getListResult(amountChargingHistoryService.getSaleStats(start, end, page), true);
    }
}
