package com.fullgoing.fullgoingapi.controller;

import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.enums.ChargeType;
import com.fullgoing.fullgoingapi.model.amountChargingHistory.AmountChargingHistoryAdminItem;
import com.fullgoing.fullgoingapi.model.amountChargingHistory.AmountChargingHistoryItem;
import com.fullgoing.fullgoingapi.model.common.CommonResult;
import com.fullgoing.fullgoingapi.model.common.ListResult;
import com.fullgoing.fullgoingapi.model.kickBoardHistory.AmountChargeHistorySaleStatsItem;
import com.fullgoing.fullgoingapi.service.amountChargingHistory.AmountChargingHistoryService;
import com.fullgoing.fullgoingapi.service.common.ResponseService;
import com.fullgoing.fullgoingapi.service.member.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@Api(tags = "충전 내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/charging-history")
public class AmountChargingHistoryController {
    private final AmountChargingHistoryService amountChargingHistoryService;
    private final ProfileService profileService;

    @ApiOperation(value = "자신의 충전 내역 확인 /회원 및 관리자")
    @GetMapping("/my/charges")
    public ListResult<AmountChargingHistoryItem> getOwnHistoryList(@RequestParam(value = "page", required = false, defaultValue = "1")int page){
        Member member = profileService.getMemberData();
        return ResponseService.getListResult(amountChargingHistoryService.getOwnChargingHistory(member, page), true);
    }

    @ApiOperation(value = "모든 충전 내역 페이지로 확인 /관리자")
    @GetMapping("/charges")
    public ListResult<AmountChargingHistoryAdminItem> searchHistoryList(@RequestParam(value = "page", required = false, defaultValue = "1")int page){
        return ResponseService.getListResult(amountChargingHistoryService.searchHistoryList(page), true);
    }
    @ApiOperation(value = "userName으로 충전 내역 검색 /관리자")
    @GetMapping("/charges/userName")
    public ListResult<AmountChargingHistoryAdminItem> searchHistoryByUserName(@RequestParam("userName") String userName, @RequestParam(value = "type", required = false) ChargeType chargeType, @RequestParam(value = "page", required = false, defaultValue = "1") int page){
        return ResponseService.getListResult(amountChargingHistoryService.searchHistoryByUserName(userName, chargeType, page), true);
    }

}
