package com.fullgoing.fullgoingapi.advice;

import com.fullgoing.fullgoingapi.enums.ResultCode;
import com.fullgoing.fullgoingapi.exception.*;
import com.fullgoing.fullgoingapi.model.common.CommonResult;
import com.fullgoing.fullgoingapi.service.common.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CAccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAccessDeniedException e) {
        return ResponseService.getFailResult(ResultCode.ACCESS_DENIED);
    }

    @ExceptionHandler(CAuthenticationEntryPointException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAuthenticationEntryPointException e) {
        return ResponseService.getFailResult(ResultCode.AUTHENTICATION_ENTRY_POINT);
    }

    @ExceptionHandler(CUsernameSignInFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CUsernameSignInFailedException e) {
        return ResponseService.getFailResult(ResultCode.USERNAME_SIGN_IN_FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CWrongPhoneNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPhoneNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PHONE_NUMBER);
    }
    @ExceptionHandler(CPutWrongCurrentPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CPutWrongCurrentPasswordException e) {
        return ResponseService.getFailResult(ResultCode.PUT_WRONG_CURRENT_PASSWORD);
    }
    @ExceptionHandler(CPutNotSameRePasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CPutNotSameRePasswordException e) {
        return ResponseService.getFailResult(ResultCode.PUT_NOT_SAME_RE_PASSWORD);
    }
    @ExceptionHandler(CWrongLicenceNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongLicenceNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_LICENCE_NUMBER);
    }
    @ExceptionHandler(CMinusRemainMoneyException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMinusRemainMoneyException e) {
        return ResponseService.getFailResult(ResultCode.MINUS_REMAIN_MONEY);
    }
    @ExceptionHandler(CExistSamePhoneNumException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CExistSamePhoneNumException e) {
        return ResponseService.getFailResult(ResultCode.EXIST_SAME_PHONE_NUM);
    }
    @ExceptionHandler(CWrongAuthNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongAuthNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_AUTH_NUMBER);
    }
    @ExceptionHandler(CAlreadyUsingKickBoardException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CAlreadyUsingKickBoardException e) {
        return ResponseService.getFailResult(ResultCode.ALREADY_USING_KICKBOARD);
    }
    @ExceptionHandler(CSomeoneUseThisKickBoardException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CSomeoneUseThisKickBoardException e) {
        return ResponseService.getFailResult(ResultCode.SOMEONE_USE_THIS_KICKBOARD);
    }
    @ExceptionHandler(CIsLicenceFalseException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CIsLicenceFalseException e) {
        return ResponseService.getFailResult(ResultCode.IS_LICENCE_FALSE);
    }
    @ExceptionHandler(CUnValidTypeOfUsernameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CUnValidTypeOfUsernameException e) {
        return ResponseService.getFailResult(ResultCode.UNVALID_TYPE_OF_USERNAME);
    }
    @ExceptionHandler(CExistSameUsernameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CExistSameUsernameException e) {
        return ResponseService.getFailResult(ResultCode.EXIST_SAME_USERNAME);
    }
    @ExceptionHandler(CExistSameModelNameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CExistSameModelNameException e) {
        return ResponseService.getFailResult(ResultCode.EXIST_SAME_MODEL_NAME);
    }
    @ExceptionHandler(CLessThanChargeMinPriceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CLessThanChargeMinPriceException e) {
        return ResponseService.getFailResult(ResultCode.LESS_THAN_CHARGE_MIN_PRICE);
    }
    @ExceptionHandler(CMoreThanChargeMaxPriceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMoreThanChargeMaxPriceException e) {
        return ResponseService.getFailResult(ResultCode.MORE_THAN_CHARGE_MAX_PRICE);
    }
    @ExceptionHandler(CAlreadyFinishUseHistoryException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CAlreadyFinishUseHistoryException e) {
        return ResponseService.getFailResult(ResultCode.ALREADY_FINISH_USE_HISTORY);
    }
    @ExceptionHandler(CExistSameLicenceNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CExistSameLicenceNumberException e) {
        return ResponseService.getFailResult(ResultCode.EXIST_SAME_LICENCE_NUMBER);
    }

    //////////////////////////////////////////게시판 예외 처리/////////////////////////////////////////////////////////

    @ExceptionHandler(CNoPermissionPutContentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoPermissionPutContentException e) {
        return ResponseService.getFailResult(ResultCode.NO_PERMISSION_PUT_CONTENT);
    }
    @ExceptionHandler(CNoPermissionDeleteContentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoPermissionDeleteContentException e) {
        return ResponseService.getFailResult(ResultCode.NO_PERMISSION_DELETE_CONTENT);
    }
}