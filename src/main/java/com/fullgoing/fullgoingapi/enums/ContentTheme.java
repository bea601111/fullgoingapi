package com.fullgoing.fullgoingapi.enums;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@ApiModel(description = "글 태그용 enum")
public enum ContentTheme {

    NORMAL("일반"),

    QUESTION("질문"),

    INFOMATION("정보"),

    IMAGETAB("이미지");

    private final String name;
}
