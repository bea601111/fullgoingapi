package com.fullgoing.fullgoingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0, "성공하였습니다.")
    , FAILED(-1, "실패하였습니다.")

    , ACCESS_DENIED(-1000, "권한이 없습니다.")
    , USERNAME_SIGN_IN_FAILED(-1001, "가입된 사용자가 아닙니다.")
    , AUTHENTICATION_ENTRY_POINT(-1002, "접근 권한이 없습니다.")

    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.")
    , WRONG_PHONE_NUMBER(-10001, "잘못된 핸드폰 번호입니다.")
    , PUT_WRONG_CURRENT_PASSWORD(-10002,"현재 비밀번호가 다릅니다.")
    , PUT_NOT_SAME_RE_PASSWORD(-10003, "바꿀 비밀번호가 동일하지 않습니다.")
    , WRONG_LICENCE_NUMBER(-10004, "올바른 면허증번호가 아닙니다.")
    , MINUS_REMAIN_MONEY(-10005, "부족한 금액을 먼저 충전해 주세요")
    , EXIST_SAME_PHONE_NUM(-10006, "동일한 전화번호가 이미 가입되어 있습니다.")
    , WRONG_AUTH_NUMBER(-10007, "인증코드가 다릅니다.")
    , MISSING_AUTH_SUCCESS(-10008, "인증이 완료되지 않았습니다.")
    , ALREADY_USING_KICKBOARD(-10009, "이미 킥보드를 사용중입니다.")
    , SOMEONE_USE_THIS_KICKBOARD(-10010, "다른 사람이 사용중인 킥보드 입니다.")
    , IS_LICENCE_FALSE(-10011, "면허증을 인증해주세요")
    , UNVALID_TYPE_OF_USERNAME(-10012, "유효한 아이디 형식이 아닙니다.")
    , EXIST_SAME_USERNAME(-10013, "이미 사용 중인 아이디 입니다.")
    , EXIST_SAME_MODEL_NAME(-10014, "이미 등록된 모델명 입니다.")
    , LESS_THAN_CHARGE_MIN_PRICE(-10015, "충전 최소 금액은 1,000원 입니다.")
    , MORE_THAN_CHARGE_MAX_PRICE(-10016, "충전 최대 금액은 1,000,000원 입니다.")
    , ALREADY_FINISH_USE_HISTORY(-10017, "이미 사용 종료된 이용내역 입니다.")
    , EXIST_SAME_LICENCE_NUMBER(-10018, "동일한 면허증 번호가 이미 가입되어 있습니다.")

    , NO_PERMISSION_PUT_CONTENT(-20000, "본인의 글만 수정할 수 있습니다.")
    , NO_PERMISSION_DELETE_CONTENT(-20000, "본인의 글만 삭할 수 있습니다.")
    ;

    private final Integer code;
    private final String msg;
}
