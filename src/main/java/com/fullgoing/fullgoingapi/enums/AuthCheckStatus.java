package com.fullgoing.fullgoingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor

public enum AuthCheckStatus {

    WAITING("대기중"),
    SUCCESS("성공"),
    FALIED("실패");

    private final String name;
}
