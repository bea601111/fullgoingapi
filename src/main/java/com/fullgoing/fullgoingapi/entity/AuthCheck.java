package com.fullgoing.fullgoingapi.entity;

import com.fullgoing.fullgoingapi.enums.AuthCheckStatus;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AuthCheck {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 13)
    private String receiveNum;

    @Column(nullable = false, length = 6)
    private String authNum;

    @Column(nullable = false)
    private Boolean authCheckStatus;

    public void putAuthCheckStatusSuccess(){
        this.authCheckStatus = true;
    }

    private AuthCheck(Builder builder){
        this.receiveNum = builder.receiveNum;
        this.authNum = builder.authNum;
        this.authCheckStatus = builder.authCheckStatus;
    }
    public static class Builder implements CommonModelBuilder<AuthCheck>{

        private final String receiveNum;
        private final String authNum;
        private final Boolean authCheckStatus;

        public Builder(String receiveNum, String authNum){
            this.receiveNum = receiveNum;
            this.authNum = authNum;
            this.authCheckStatus = false;
        }

        @Override
        public AuthCheck build() {
            return new AuthCheck(this);
        }
    }
}
