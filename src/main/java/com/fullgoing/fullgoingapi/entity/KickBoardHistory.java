package com.fullgoing.fullgoingapi.entity;

import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.model.kickboard.KickBoardUseEndRequest;
import com.fullgoing.fullgoingapi.model.kickboard.KickBoardUseStartRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardHistory {
    //시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kickBoardId", nullable = false)
    private KickBoard kickBoard;

    // 시작 위도
    @Column(nullable = false)
    private Double startPosX;

    // 시작 경도
    @Column(nullable = false)
    private Double startPosY;

    //시작 시간
    @Column(nullable = false)
    private LocalDateTime dateStart;

    // 종료 위도
    private Double endPosX;

    // 종료 경도
    private Double endPosY;

    // 종료 시간
    private LocalDateTime dateEnd;

    // 결제 금액
    @Column(nullable = false)
    private Double resultPrice;

    // 결제 패스
    @Column(nullable = false)
    private Double resultPass;

    // 완료 여부 (결제 완료 여부)
    @Column(nullable = false)
    private Boolean isComplete;

    public void putResultPricePass(Double resultPrice, Double resultPass){
        this.resultPrice = resultPrice;
        this.resultPass = resultPass;
        this.isComplete = true;
    }

    public void putUseEnd(KickBoardUseEndRequest kickBoardUseEndRequest){
        this.endPosX = kickBoardUseEndRequest.getEndPosX();
        this.endPosY = kickBoardUseEndRequest.getEndPosY();
        this.dateEnd = LocalDateTime.now();
    }


    public void putResultPrice(Double resultPrice, Double resultPass){
        this.resultPrice = resultPrice;
        this.resultPass = resultPass;
        this.isComplete = true;
    }

    private KickBoardHistory(Builder builder) {
        this.member = builder.member;
        this.kickBoard = builder.kickBoard;
        this.startPosX = builder.startPosX;
        this.startPosY = builder.startPosY;
        this.dateStart = builder.dateStart;
        this.resultPrice = builder.resultPrice;
        this.isComplete = builder.isComplete;
        this.resultPass = builder.resultPass;
    }


    public static class Builder implements CommonModelBuilder<KickBoardHistory> {
        private final Member member;
        private final KickBoard kickBoard;
        private final Double startPosX;
        private final Double startPosY;
        private final LocalDateTime dateStart;
        private final Double resultPrice;
        private final Boolean isComplete;
        private final Double resultPass;

        public Builder(Member member, KickBoard kickBoard, KickBoardUseStartRequest kickBoardUseStartRequest){
            this.member = member;
            this.kickBoard = kickBoard;
            this.startPosX = kickBoardUseStartRequest.getStartPosX();
            this.startPosY = kickBoardUseStartRequest.getStartPosY();
            this.dateStart = LocalDateTime.now();
            this.resultPrice = 0D;
            this.isComplete = false;
            this.resultPass = 0D;
        }
        @Override
        public KickBoardHistory build() {
            return new KickBoardHistory(this);
        }
    }




}
