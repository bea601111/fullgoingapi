package com.fullgoing.fullgoingapi.entity;

import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.lib.CommonFormat;
import com.fullgoing.fullgoingapi.model.messageBoard.MessageBoardRequest;
import com.fullgoing.fullgoingapi.model.messageBoard.MessageBoardTitleNameImgUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MessageBoard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 50)
    private String title;

    @Column(nullable = false)
    private LocalDateTime wroteDateTime;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String context;

    private String imgName;



    public void putTitleThemeContext(MessageBoardTitleNameImgUpdateRequest updateRequest){
        this.title = updateRequest.getTitle();
        this.context = updateRequest.getContext();
        this.name = updateRequest.getName();
        this.imgName = updateRequest.getImgName();
    }

    private MessageBoard(Builder builder){
        this.name = builder.name;
        this.title = builder.title;
        this.wroteDateTime = builder.wroteDateTime;
        this.context = builder.context;
        this.imgName = builder.imgName;
    }
    public static class Builder implements CommonModelBuilder<MessageBoard>{

        private final String name;
        private final String title;
        private final LocalDateTime wroteDateTime;
        private final String context;
        private final String imgName;

        public Builder(MessageBoardRequest messageBoardRequest){
            this.name = messageBoardRequest.getName();
            this.title = messageBoardRequest.getTitle();
            this.wroteDateTime = LocalDateTime.now();
            this.context = messageBoardRequest.getContext();
            this.imgName = messageBoardRequest.getName();
        }

        @Override
        public MessageBoard build() {
            return new MessageBoard(this);
        }
    }
}
