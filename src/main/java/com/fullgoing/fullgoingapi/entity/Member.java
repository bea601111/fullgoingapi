package com.fullgoing.fullgoingapi.entity;

import com.fullgoing.fullgoingapi.enums.MemberGroup;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.model.member.MemberCreateRequest;
import com.fullgoing.fullgoingapi.model.member.MemberJoinRequest;
import com.fullgoing.fullgoingapi.model.member.MemberLicenceNumUpdateRequest;
import com.fullgoing.fullgoingapi.model.member.MemberPasswordPhoneNumUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member implements UserDetails {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 그룹
    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private MemberGroup memberGroup;

    // 아이디
    @Column(nullable = false, unique = true, length = 30)
    private String username;

    // 비밀번호
    @Column(nullable = false)
    private String password;

    // 이름
    @Column(nullable = false, length = 20)
    private String name;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    // 사용 유무
    @Column(nullable = false)
    private Boolean isEnabled;

    // 연락처
    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    // 면허등록 유무
    @Column(nullable = false)
    private Boolean isLicence;

    // 면허번호
    @Column(length = 15)
    private String licenceNumber;

    public void putPassword(String password){
        this.password = password;
    }

    public void putPhoneNumPassword(MemberPasswordPhoneNumUpdateRequest memberPasswordPhoneNumUpdateRequest){
        this.password = memberPasswordPhoneNumUpdateRequest.getNewPassword();
    }

    public void setLicenceNumber(MemberLicenceNumUpdateRequest licenceNumber){
        this.isLicence = true;
        this.licenceNumber = licenceNumber.getLicenceNumber();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(memberGroup.toString()));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled;
    }

    private Member(MemberBuilder builder) {
        this.memberGroup = builder.memberGroup;
        this.username = builder.username;
        this.password = builder.password;
        this.name = builder.name;
        this.dateCreate = builder.dateCreate;
        this.isEnabled = builder.isEnabled;
        this.isLicence = builder.isLicence;
        this.phoneNumber = builder.phoneNumber;
    }
    private Member(AdminBuilder builder) {
        this.memberGroup = builder.memberGroup;
        this.username = builder.username;
        this.password = builder.password;
        this.name = builder.name;
        this.dateCreate = builder.dateCreate;
        this.isEnabled = builder.isEnabled;
        this.licenceNumber = builder.licenceNumber;
        this.isLicence = builder.isLicence;
        this.phoneNumber = builder.phoneNumber;
    }

    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final MemberGroup memberGroup;
        private final String username;
        private final String password;
        private final String name;
        private final LocalDateTime dateCreate;
        private final Boolean isEnabled;
        private final String phoneNumber;
        private final Boolean isLicence;

        // 빌더에서 회원그룹 따로 받는 이유 : 일반유저가 회원가입시 내가 일반유저다 라고 선택하지 않음.
        // 회원등록은 관리자페이지에서 관리자가 하거나 일반유저가 회원가입하거나.. N개의 경우의 수가 존재함.
        public MemberBuilder(MemberGroup memberGroup, MemberCreateRequest createRequest) {
            this.memberGroup = memberGroup;
            this.username = createRequest.getUsername();
            this.password = createRequest.getPassword();
            this.name = createRequest.getName();
            this.dateCreate = LocalDateTime.now();
            this.isEnabled = true;
            this.isLicence = false;
            this.phoneNumber = createRequest.getPhoneNumber();
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
    public static class AdminBuilder implements CommonModelBuilder<Member> {
        private final MemberGroup memberGroup;
        private final String username;
        private final String password;
        private final String name;
        private final LocalDateTime dateCreate;
        private final Boolean isEnabled;
        private final String phoneNumber;
        private final Boolean isLicence;
        private final String licenceNumber;

        // 빌더에서 회원그룹 따로 받는 이유 : 일반유저가 회원가입시 내가 일반유저다 라고 선택하지 않음.
        // 회원등록은 관리자페이지에서 관리자가 하거나 일반유저가 회원가입하거나.. N개의 경우의 수가 존재함.
        public AdminBuilder(MemberGroup memberGroup, MemberJoinRequest memberJoinRequest) {
            this.memberGroup = memberGroup;
            this.username = memberJoinRequest.getUsername();
            this.password = memberJoinRequest.getPassword();
            this.name = memberJoinRequest.getName();
            this.dateCreate = LocalDateTime.now();
            this.licenceNumber = memberJoinRequest.getLicenceNumber().isEmpty() || memberJoinRequest.getLicenceNumber() == null ? null : memberJoinRequest.getLicenceNumber();
            this.phoneNumber = memberJoinRequest.getPhoneNumber();
            this.isEnabled = true;
            this.isLicence = memberJoinRequest.getLicenceNumber() == null ? false : true;
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
