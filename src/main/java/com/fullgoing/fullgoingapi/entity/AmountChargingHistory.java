package com.fullgoing.fullgoingapi.entity;

import com.fullgoing.fullgoingapi.enums.ChargeType;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.model.amountChargingHistory.AmountChargingHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AmountChargingHistory {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // memberId
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    // 충전 타입
    @Enumerated(value = EnumType.STRING)
    @Column(length = 20, nullable = false)
    private ChargeType chargeType;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateReg;

    // 충전액
    @Column(nullable = false)
    private Double charge;

    // 사용 마일리지
    @Column(nullable = false)
    private Double resultMileage;

    // 결제 금액
    @Column(nullable = false)
    private Double paymentAmount;

    private AmountChargingHistory(MileageBuilder mileageBuilder){
        this.member = mileageBuilder.member;
        this.chargeType = mileageBuilder.chargeType;
        this.dateReg = mileageBuilder.dateReg;
        this.charge = mileageBuilder.charge;
        this.resultMileage = mileageBuilder.resultMileage;
        this.paymentAmount = mileageBuilder.paymentAmount;
    }
    private AmountChargingHistory(Builder builder){
        this.member = builder.member;
        this.chargeType = builder.chargeType;
        this.dateReg = builder.dateReg;
        this.charge = builder.charge;
        this.resultMileage = builder.resultMileage;
        this.paymentAmount = builder.paymentAmount;
    }
    private AmountChargingHistory(PriceBuilder priceBuilder){
        this.member = priceBuilder.member;
        this.chargeType = priceBuilder.chargeType;
        this.dateReg = priceBuilder.dateReg;
        this.charge = priceBuilder.charge;
        this.resultMileage = priceBuilder.resultMileage;
        this.paymentAmount = priceBuilder.paymentAmount;
    }
    private AmountChargingHistory(PassBuilder passBuilder){
        this.member = passBuilder.member;
        this.chargeType = passBuilder.chargeType;
        this.dateReg = passBuilder.dateReg;
        this.charge = passBuilder.charge;
        this.resultMileage = passBuilder.resultMileage;
        this.paymentAmount = passBuilder.paymentAmount;
    }
    public static class Builder implements CommonModelBuilder<AmountChargingHistory>{

        private final Member member;
        private final ChargeType chargeType;
        private final LocalDateTime dateReg;
        private final Double charge;
        private final Double resultMileage;
        private final Double paymentAmount;

        public Builder(Member member, AmountChargingHistoryRequest historyRequest){
            this.member = member;
            this.chargeType = historyRequest.getChargeType();
            this.dateReg = LocalDateTime.now();
            this.charge = historyRequest.getCharge();
            this.resultMileage = historyRequest.getResultMileage();
            this.paymentAmount = charge - resultMileage;
        }

        @Override
        public AmountChargingHistory build() {
            return new AmountChargingHistory(this);
        }
    }

    public static class MileageBuilder implements CommonModelBuilder<AmountChargingHistory>{

        private final Member member;
        private final ChargeType chargeType;
        private final LocalDateTime dateReg;
        private final Double charge;
        private final Double resultMileage;
        private final Double paymentAmount;

        public MileageBuilder(Member member, Double mileage){
            this.member = member;
            this.chargeType = ChargeType.MILEAGE;
            this.dateReg = LocalDateTime.now();
            this.charge = mileage;
            this.resultMileage = 0D;
            this.paymentAmount = 0D;
        }

        @Override
        public AmountChargingHistory build() {
            return new AmountChargingHistory(this);
        }
    }
    public static class PriceBuilder implements CommonModelBuilder<AmountChargingHistory>{

        private final Member member;
        private final ChargeType chargeType;
        private final LocalDateTime dateReg;
        private final Double charge;
        private final Double resultMileage;
        private final Double paymentAmount;

        public PriceBuilder(Member member, Double price, Double mileage){
            this.member = member;
            this.chargeType = ChargeType.PRICE;
            this.dateReg = LocalDateTime.now();
            this.charge = price;
            this.resultMileage = mileage;
            this.paymentAmount = price - mileage;
        }
        public PriceBuilder(Member member, Double price){
            this.member = member;
            this.chargeType = ChargeType.PRICE;
            this.dateReg = LocalDateTime.now();
            this.charge = price;
            this.resultMileage = 0D;
            this.paymentAmount = 0D;
        }

        @Override
        public AmountChargingHistory build() {
            return new AmountChargingHistory(this);
        }
    }
    public static class PassBuilder implements CommonModelBuilder<AmountChargingHistory>{

        private final Member member;
        private final ChargeType chargeType;
        private final LocalDateTime dateReg;
        private final Double charge;
        private final Double resultMileage;
        private final Double paymentAmount;

        public PassBuilder(Member member, Double minutes, Double price){
            this.member = member;
            this.chargeType = ChargeType.PASS;
            this.dateReg = LocalDateTime.now();
            this.charge = minutes;
            this.resultMileage = 0D;
            this.paymentAmount = price;
        }
        public PassBuilder(Member member, Double minutes){
            this.member = member;
            this.chargeType = ChargeType.PASS;
            this.dateReg = LocalDateTime.now();
            this.charge = minutes;
            this.resultMileage = 0D;
            this.paymentAmount = 0D;
        }

        @Override
        public AmountChargingHistory build() {
            return new AmountChargingHistory(this);
        }
    }

}
