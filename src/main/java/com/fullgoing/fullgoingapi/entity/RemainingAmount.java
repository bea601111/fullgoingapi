package com.fullgoing.fullgoingapi.entity;

import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RemainingAmount {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // memberId
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    // 잔여 마일리지
    @Column(nullable = false)
    private Double remainMileage;

    // 잔여 패스
    @Column(nullable = false)
    private Double remainPass;

    //잔여 금액
    @Column(nullable = false)
    private Double remainPrice;

    public void putRemainPriceByAdmin(Double price){
        this.remainPrice += price;
    }

    public void putRemainPriceMileage(Double price, Double mileage){
        this.remainPrice += price;
        this.remainMileage = mileage;
    }

    public void putPlusPass(Double pass){
        this.remainPass += pass;
    }

    public void putRemainPricePass(double price, double pass, double mileage){
        this.remainPrice -= price;
        this.remainPass -= pass;
        this.remainMileage += mileage;
    }


    private RemainingAmount(Builder builder){
        this.member = builder.member;
        this.remainMileage = builder.remainMileage;
        this.remainPass = builder.remainPass;
        this.remainPrice = builder.remainPrice;
    }
    public static class Builder implements CommonModelBuilder<RemainingAmount>{

        private final Member member;
        private final Double remainMileage;
        private final Double remainPass;
        private final Double remainPrice;

        public Builder(Member member){
            this.member = member;
            this.remainMileage = 0D;
            this.remainPass = 0D;
            this.remainPrice = 0D;
        }

        @Override
        public RemainingAmount build() {
            return new RemainingAmount(this);
        }
    }
}
