package com.fullgoing.fullgoingapi.model.kickboard;

import com.fullgoing.fullgoingapi.enums.KickBoardStatus;
import com.fullgoing.fullgoingapi.enums.PriceBasis;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel(description = "킥보드 정보 변경용 리퀘스트")
public class KickBoardNamePriceXYMemoIsUseUpdateRequest {

    @ApiModelProperty(notes = "모델 명 / 1 ~ 20자", required = true)
    @NotNull
    @Length(min = 1, max = 20)
    private String modelName;

    @ApiModelProperty(notes = "가격기준코드", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private PriceBasis priceBasis;

    @ApiModelProperty(notes = "위도", required = true)
    @NotNull
    private Double posX;

    @ApiModelProperty(notes = "경도", required = true)
    @NotNull
    private Double posY;

    @ApiModelProperty(notes = "메모")
    private String memo;
}
