package com.fullgoing.fullgoingapi.model.kickboard;

import com.fullgoing.fullgoingapi.enums.KickBoardStatus;
import com.fullgoing.fullgoingapi.enums.PriceBasis;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@ApiModel(description = "킥보드 빌더용 리퀘스트")
public class KickBoardsRequest {

    @ApiModelProperty(notes = "모델 명 / 3 ~ 20자리", required = true)
    @NotNull
    @Length(min = 3, max = 20)
    private String modelName;

    @ApiModelProperty(notes = "가격기준코드", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private PriceBasis priceBasis;

    @ApiModelProperty(notes = "구매일", required = true)
    @NotNull
    private LocalDate dateBuy;

    @ApiModelProperty(notes = "위도", required = true)
    @NotNull
    private Double posX;

    @ApiModelProperty(notes = "경도", required = true)
    @NotNull
    private Double posY;

    @ApiModelProperty(notes = "킥보드 상태", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private KickBoardStatus kickBoardStatus;
}
