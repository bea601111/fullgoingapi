package com.fullgoing.fullgoingapi.model.kickboard;

import com.fullgoing.fullgoingapi.entity.KickBoard;
import com.fullgoing.fullgoingapi.enums.KickBoardStatus;
import com.fullgoing.fullgoingapi.enums.PriceBasis;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ApiModel(description = "킥보드 단일 정보 표기용 리스폰스")
public class KickBoardResponse {

    @ApiModelProperty(notes = "모델 명")
    private String modelName;

    @ApiModelProperty(notes = "가격기준코드")
    private PriceBasis priceBasis;

    @ApiModelProperty(notes = "위도")
    private Double posX;

    @ApiModelProperty(notes = "경도")
    private Double posY;

    @ApiModelProperty(notes = "비고")
    private String memo;

    @ApiModelProperty(notes = "킥보드 상태")
    private String kickBoardStatusName;

    private KickBoardResponse(Builder builder){
        this.modelName = builder.modelName;
        this.priceBasis = builder.priceBasis;
        this.posX = builder.posX;
        this.posY = builder.posY;
        this.memo = builder.memo;
        this.kickBoardStatusName = builder.kickBoardStatusName;
    }
    public static class Builder implements CommonModelBuilder<KickBoardResponse>{

        private final String modelName;
        private final PriceBasis priceBasis;
        private final Double posX;
        private final Double posY;
        private final String memo;
        private final String kickBoardStatusName;

        public Builder(KickBoard kickBoard){
            this.modelName = kickBoard.getModelName();
            this.priceBasis = kickBoard.getPriceBasis();
            this.posX = kickBoard.getPosX();
            this.posY = kickBoard.getPosY();
            this.memo = kickBoard.getMemo();
            this.kickBoardStatusName = kickBoard.getKickBoardStatus().getName();
        }

        @Override
        public KickBoardResponse build() {
            return new KickBoardResponse(this);
        }
    }
}
