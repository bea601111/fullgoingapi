package com.fullgoing.fullgoingapi.model.kickboard;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel(description = "킥보드 사용 종료용 리퀘스트")
public class KickBoardUseEndRequest {
    // 종료 위도
    @ApiModelProperty(notes = "종료 위도 / 음수 허용", required = true)
    @NotNull
    private Double endPosX;

    // 종료 경도
    @ApiModelProperty(notes = "종료 경도 / 음수 허용", required = true)
    @NotNull
    private Double endPosY;
}
