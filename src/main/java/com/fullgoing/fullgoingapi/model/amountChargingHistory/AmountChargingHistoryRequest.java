package com.fullgoing.fullgoingapi.model.amountChargingHistory;

import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.enums.ChargeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@ApiModel(description = "빌더용 충전 내역 리퀘스트")
public class AmountChargingHistoryRequest {

    @ApiModelProperty(notes = "고객FK", required = true)
    @NotNull
    private Member member;

    @ApiModelProperty(notes = "충전 타입", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ChargeType chargeType;

    @ApiModelProperty(notes = "등록 일자", required = true)
    @NotNull
    private LocalDateTime dateReg;

    @ApiModelProperty(notes = "충전 금액/마일리지/패스 / 0 ~ 1000000", required = true)
    @NotNull
    @Min(0)
    @Max(1000000)
    private Double charge;

    @ApiModelProperty(notes = "사용 마일리지 / 0 ~ 1000000", required = true)
    @NotNull
    @Min(0)
    @Max(1000000)
    private Double resultMileage;

    @ApiModelProperty(notes = "총 결제금액 / 0 ~ 1000000", required = true)
    @NotNull
    @Min(0)
    @Max(1000000)
    private Double paymentAmount;
}
