package com.fullgoing.fullgoingapi.model.amountChargingHistory;

import com.fullgoing.fullgoingapi.entity.AmountChargingHistory;
import com.fullgoing.fullgoingapi.enums.ChargeType;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.lib.CommonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ApiModel(description = "고객용 충전 내역 아이템")
public class AmountChargingHistoryItem {

    @ApiModelProperty(notes = "충전 타입")
    private String chargeType;

    @ApiModelProperty(notes = "등록 일자")
    private String dateReg;

    @ApiModelProperty(notes = "충전 금액/마일리지/패스")
    private BigDecimal charge;

    @ApiModelProperty(notes = "사용 마일리지")
    private BigDecimal resultMileage;

    @ApiModelProperty(notes = "총 결제 금액")
    private BigDecimal paymentAmount;

    private AmountChargingHistoryItem(Builder builder){
        this.chargeType = builder.chargeType;
        this.dateReg = builder.dateReg;
        this.charge = builder.charge;
        this.resultMileage = builder.resultMileage;
        this.paymentAmount = builder.paymentAmount;
    }
    public static class Builder implements CommonModelBuilder<AmountChargingHistoryItem>{

        private final String chargeType;
        private final String dateReg;
        private final BigDecimal charge;
        private final BigDecimal resultMileage;
        private final BigDecimal paymentAmount;

        public Builder(AmountChargingHistory amountChargingHistory){
            this.chargeType = amountChargingHistory.getChargeType().getName();
            this.dateReg = CommonFormat.convertLocalDateTimeToString(amountChargingHistory.getDateReg());
            this.charge = CommonFormat.convertDoubleToDecimal(amountChargingHistory.getCharge());
            this.resultMileage = CommonFormat.convertDoubleToDecimal(amountChargingHistory.getResultMileage());
            this.paymentAmount = CommonFormat.convertDoubleToDecimal(amountChargingHistory.getPaymentAmount());
        }

        @Override
        public AmountChargingHistoryItem build() {
            return new AmountChargingHistoryItem(this);
        }
    }
}
