package com.fullgoing.fullgoingapi.model.amountChargingHistory;

import com.fullgoing.fullgoingapi.entity.AmountChargingHistory;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.lib.CommonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ApiModel(description = "관리자용 충전 내역 아이템")
public class AmountChargingHistoryAdminItem {

    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "아이디")
    private String userName;

    @ApiModelProperty(notes = "충전 타입")
    private String chargeType;

    @ApiModelProperty(notes = "등록 일자")
    private String dateReg;

    @ApiModelProperty(notes = "충전 금액/마일리지/패스")
    private BigDecimal charge;

    @ApiModelProperty(notes = "사용 마일리지")
    private BigDecimal resultMileage;

    @ApiModelProperty(notes = "총 결제 금액")
    private BigDecimal paymentAmount;

    private AmountChargingHistoryAdminItem(AdminBuilder adminBuilder){
        this.id = adminBuilder.id;
        this.userName = adminBuilder.userName;
        this.chargeType = adminBuilder.chargeType;
        this.dateReg = adminBuilder.dateReg;
        this.charge = adminBuilder.charge;
        this.resultMileage = adminBuilder.resultMileage;
        this.paymentAmount = adminBuilder.paymentAmount;
    }
    public static class AdminBuilder implements CommonModelBuilder<AmountChargingHistoryAdminItem> {

        private final Long id;
        private final String userName;
        private final String chargeType;
        private final String dateReg;
        private final BigDecimal charge;
        private final BigDecimal resultMileage;
        private final BigDecimal paymentAmount;

        public AdminBuilder(AmountChargingHistory amountChargingHistory){
            this.id = amountChargingHistory.getId();
            this.userName = amountChargingHistory.getMember().getUsername();
            this.chargeType = amountChargingHistory.getChargeType().getName();
            this.dateReg = CommonFormat.convertLocalDateTimeToString(amountChargingHistory.getDateReg());
            this.charge = CommonFormat.convertDoubleToDecimal(amountChargingHistory.getCharge());
            this.resultMileage = CommonFormat.convertDoubleToDecimal(amountChargingHistory.getResultMileage());
            this.paymentAmount = CommonFormat.convertDoubleToDecimal(amountChargingHistory.getPaymentAmount());
        }

        @Override
        public AmountChargingHistoryAdminItem build() {
            return new AmountChargingHistoryAdminItem(this);
        }
    }
}
