package com.fullgoing.fullgoingapi.model.messageBoard;

import com.fullgoing.fullgoingapi.entity.MessageBoard;
import com.fullgoing.fullgoingapi.enums.ContentTheme;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.lib.CommonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ApiModel(description = "게시글 일람용 리스폰스")
public class MessageBoardResponse {

    @ApiModelProperty(name = "회원명")
    private String name;

    @ApiModelProperty(name = "제목")
    private String title;

    @ApiModelProperty(name = "작성 날짜")
    private String wroteDateTime;

    @ApiModelProperty(name = "본문")
    private String context;

    @ApiModelProperty(name = "이미지 주소")
    private String imgName;

    private MessageBoardResponse(Builder builder){
        this.name = builder.name;
        this.title = builder.title;
        this.wroteDateTime = builder.wroteDateTime;
        this.context = builder.context;
        this.imgName = builder.imgName;
    }
    public static class Builder implements CommonModelBuilder<MessageBoardResponse>{

        private final String name;
        private final String title;
        private final String wroteDateTime;
        private final String context;
        private final String imgName;

        public Builder(MessageBoard messageBoard){
            this.name = messageBoard.getName();
            this.title = messageBoard.getTitle();
            this.wroteDateTime = CommonFormat.convertLocalDateTimeToString(messageBoard.getWroteDateTime());
            this.context = messageBoard.getContext();
            this.imgName = messageBoard.getImgName();
        }

        @Override
        public MessageBoardResponse build() {
            return new MessageBoardResponse(this);
        }
    }
}
