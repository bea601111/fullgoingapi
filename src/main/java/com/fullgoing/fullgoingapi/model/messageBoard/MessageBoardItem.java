package com.fullgoing.fullgoingapi.model.messageBoard;

import com.fullgoing.fullgoingapi.entity.MessageBoard;
import com.fullgoing.fullgoingapi.enums.ContentTheme;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.lib.CommonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ApiModel(description = "게시판 리스트용 아이템")
public class MessageBoardItem {

    @ApiModelProperty(name = "번호")
    private Long id;

    @ApiModelProperty(name = "회원명")
    private String name;

    @ApiModelProperty(name = "제목")
    private String title;

    @ApiModelProperty(name = "작성 날짜")
    private String wroteDateTime;

    private MessageBoardItem(Builder builder){
        this.id = builder.id;
        this.name = builder.name;
        this.title = builder.title;
        this.wroteDateTime = builder.wroteDateTime;
    }
    public static class Builder implements CommonModelBuilder<MessageBoardItem>{

        private final Long id;
        private final String name;
        private final String title;
        private final String wroteDateTime;


        public Builder(MessageBoard messageBoard){
            this.id = messageBoard.getId();
            this.name = messageBoard.getName();
            this.title = messageBoard.getTitle();
            this.wroteDateTime = CommonFormat.convertLocalDateTimeToString(messageBoard.getWroteDateTime());
        }

        @Override
        public MessageBoardItem build() {
            return new MessageBoardItem(this);
        }
    }

}
