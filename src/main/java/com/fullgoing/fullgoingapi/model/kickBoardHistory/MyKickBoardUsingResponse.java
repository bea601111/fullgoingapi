package com.fullgoing.fullgoingapi.model.kickBoardHistory;

import com.fullgoing.fullgoingapi.entity.KickBoardHistory;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.lib.CommonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ApiModel(description = "자신용 사용중인 킥보드 내용 아이템")
public class MyKickBoardUsingResponse {

    @ApiModelProperty(notes = "킥보드 이용내역 시퀀스")
    private Long historyId; // 없으면 0

    @ApiModelProperty(notes = "킥보드 모델 명")
    private String kickBoardModelName; // 없으면 ""

    @ApiModelProperty(notes = "가격 기준 코드 명")
    private String priceName; // 없으면 ""

    @ApiModelProperty(notes = "기본 이용 가격")
    private BigDecimal priceBase; // 0

    @ApiModelProperty(notes = "분 당 이용 가격")
    private BigDecimal priceMinute; // 0

    @ApiModelProperty(notes = "위도")
    private Double startPosX; // 없으면 0

    @ApiModelProperty(notes = "경도")
    private Double startPosY; // 없으면 0

    @ApiModelProperty(notes = "사용 시작 일시")
    private String dateStart; // 없으면 ""

    private MyKickBoardUsingResponse(Builder builder){
        this.historyId = builder.historyId;
        this.kickBoardModelName = builder.kickBoardModelName;
        this.startPosX = builder.startPosX;
        this.startPosY = builder.startPosY;
        this.dateStart = builder.dateStart;
        this.priceName = builder.priceName;
        this.priceBase = builder.priceBase;
        this.priceMinute = builder.priceMinute;
    }
    public static class Builder implements CommonModelBuilder<MyKickBoardUsingResponse>{

        private final Long historyId;
        private final String kickBoardModelName;
        private final Double startPosX;
        private final Double startPosY;
        private final String dateStart;
        private final String priceName;
        private final BigDecimal priceBase;
        private final BigDecimal priceMinute;

        public Builder(Long historyId, String kickBoardModelName, Double startPosX, Double startPosY, String dateStart, String priceName, Double priceBase, Double priceMinute){
            this.historyId = historyId;
            this.kickBoardModelName = kickBoardModelName;
            this.startPosX = startPosX;
            this.startPosY = startPosY;
            this.dateStart = dateStart;
            this.priceName = priceName;
            this.priceBase = CommonFormat.convertDoubleToDecimal(priceBase);
            this.priceMinute = CommonFormat.convertDoubleToDecimal(priceMinute);
        }

        @Override
        public MyKickBoardUsingResponse build() {
            return new MyKickBoardUsingResponse(this);
        }
    }
}
