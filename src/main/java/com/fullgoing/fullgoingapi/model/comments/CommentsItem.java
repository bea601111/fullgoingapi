package com.fullgoing.fullgoingapi.model.comments;

import com.fullgoing.fullgoingapi.entity.Comments;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.lib.CommonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ApiModel(description = "댓글 내역용 아이템")
public class CommentsItem {

    @ApiModelProperty(notes = "댓글 시퀀스")
    private Long commentId;

    @ApiModelProperty(notes = "이름")
    private String name;

    @ApiModelProperty(notes = "본문")
    private String userComments;

    @ApiModelProperty(notes = "작성 일시")
    private String wroteCommentTime;

    private CommentsItem(Builder builder){
        this.commentId = builder.commentId;
        this.name = builder.name;
        this.userComments = builder.userComments;
        this.wroteCommentTime = builder.wroteCommentTime;
    }
    public static class Builder implements CommonModelBuilder<CommentsItem>{

        private final Long commentId;
        private final String name;
        private final String userComments;
        private final String wroteCommentTime;

        public Builder(Comments comments){
            this.commentId = comments.getId();
            this.name = comments.getName();
            this.userComments = comments.getUserComment();
            this.wroteCommentTime = CommonFormat.convertLocalDateTimeToString(comments.getWroteCommentDate());
        }

        @Override
        public CommentsItem build() {
            return new CommentsItem(this);
        }
    }
}
