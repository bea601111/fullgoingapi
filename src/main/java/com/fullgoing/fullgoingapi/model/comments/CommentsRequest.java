package com.fullgoing.fullgoingapi.model.comments;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel(description = "댓글 작성용 리퀘스트")
public class CommentsRequest {

    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "이름", required = true)
    private String name;

    @NotNull
    @Length(min = 1, max = 100)
    @ApiModelProperty(notes = "댓글 본문", required = true)
    private String userComments;
}
