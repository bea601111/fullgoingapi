package com.fullgoing.fullgoingapi.model.member;

import com.fullgoing.fullgoingapi.entity.MessageSendHistory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "인증번호 발신용 리스폰스")
public class AuthCheckCodeResponse {

    @ApiModelProperty(notes = "인증번호")
    private String code;
}
