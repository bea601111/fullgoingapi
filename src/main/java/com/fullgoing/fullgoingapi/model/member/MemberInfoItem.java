package com.fullgoing.fullgoingapi.model.member;

import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.entity.RemainingAmount;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.lib.CommonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ApiModel(description = "고객 정보 확인용 아이템")
public class MemberInfoItem {

    @ApiModelProperty(notes = "멤버 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "아이디")
    private String username;

    @ApiModelProperty(notes = "고객 명")
    private String name;

    @ApiModelProperty(notes = "가입일")
    private String dateCreate;

    @ApiModelProperty(notes = "전화번호")
    private String phoneNumber;

    @ApiModelProperty(notes = "면허 소지 여부")
    private Boolean isLicence;

    private MemberInfoItem(Builder builder){
        this.memberId = builder.memberId;
        this.username = builder.username;
        this.name = builder.name;
        this.dateCreate = builder.dateCreate;
        this.phoneNumber = builder.phoneNumber;
        this.isLicence = builder.isLicence;
    }
    public static class Builder implements CommonModelBuilder<MemberInfoItem>{
        private final Long memberId;
        private final String username;
        private final String name;
        private final String dateCreate;
        private final String phoneNumber;
        private final Boolean isLicence;


        public Builder(RemainingAmount remainingAmount){
            this.memberId = remainingAmount.getMember().getId();
            this.username = remainingAmount.getMember().getUsername();
            this.name = remainingAmount.getMember().getName();
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(remainingAmount.getMember().getDateCreate());
            this.phoneNumber = remainingAmount.getMember().getPhoneNumber();
            this.isLicence = remainingAmount.getMember().getIsLicence();
        }

        @Override
        public MemberInfoItem build() {
            return new MemberInfoItem(this);
        }
    }
}
