package com.fullgoing.fullgoingapi.model.member;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel(description = "인증 확인용 리퀘스트")
public class MemberAuthCheckRequest {
    @ApiModelProperty(notes = "연락처 / 13자리", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String phoneNumber;

    @ApiModelProperty(notes = "인증번호 / 6자리", required = true)
    @NotNull
    @Length(min = 6, max = 6)
    private String authNum;
}
