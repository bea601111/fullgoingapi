package com.fullgoing.fullgoingapi.model.member;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel(description = "면허증 등록용 리퀘스트")
public class MemberLicenceNumUpdateRequest {

    @ApiModelProperty(notes = "면허증번호 / 15자리", required = true)
    @NotNull
    @Length(min = 15, max = 15)
    private String licenceNumber;
}
