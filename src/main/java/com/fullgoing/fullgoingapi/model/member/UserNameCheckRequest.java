package com.fullgoing.fullgoingapi.model.member;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel(description = "아이디 중복확인용 리퀘스트")
public class UserNameCheckRequest {

    @ApiModelProperty(notes = "아이디 / 5 ~ 30자리", required = true)
    @NotNull
    @Length(min = 5, max = 30)
    private String userName;
}
