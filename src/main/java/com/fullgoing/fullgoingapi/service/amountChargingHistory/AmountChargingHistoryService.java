package com.fullgoing.fullgoingapi.service.amountChargingHistory;

import com.fullgoing.fullgoingapi.entity.AmountChargingHistory;
import com.fullgoing.fullgoingapi.entity.KickBoardHistory;
import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.enums.ChargeType;
import com.fullgoing.fullgoingapi.enums.PassType;
import com.fullgoing.fullgoingapi.model.amountChargingHistory.AmountChargingHistoryAdminItem;
import com.fullgoing.fullgoingapi.model.amountChargingHistory.AmountChargingHistoryItem;
import com.fullgoing.fullgoingapi.model.common.ListResult;
import com.fullgoing.fullgoingapi.model.kickBoardHistory.AmountChargeHistorySaleStatsItem;
import com.fullgoing.fullgoingapi.repository.AmountChargingHistoryRepository;
import com.fullgoing.fullgoingapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AmountChargingHistoryService { //충전 내역을 저장하는 서비스
    private final AmountChargingHistoryRepository amountChargingHistoryRepository;

    public void plusMileage(Member member, Double mileage) { // 사용 종료 후, 마일리지 적립.
        AmountChargingHistory amountChargingHistory = new AmountChargingHistory.MileageBuilder(member, mileage).build();
        //킥보드 사용이 종료 되었을 때, 계산된 마일리지를 전달 받아, 사용자의 보유 마일리지에 더해주도록 빌더에게 넘겨준다.
        amountChargingHistoryRepository.save(amountChargingHistory); // repository에 해당 값을 저장하도록 시킨다.
    }

    public void useMileageForChargePrice(Member member, Double price, Double mileage) { // 사용자의 토큰 값을 받아 충전된 금액과 소모한 마일리지를 기록하는 메서드
        if (price < mileage) { // 사용자가 소모한 마일리지가 충전한 금액보다 많다면, 마일리지를 지불하는 금액만큼만 소비해야하니
            mileage = price; // 마일리지를 지불하는 금액과 동일하게 만들어준다.
        }
        AmountChargingHistory amountChargingHistory = new AmountChargingHistory.PriceBuilder(member, price, mileage).build();
        // 빌더에게 price(충전한 금액/결제금액)와 소비한 마일리지를 넘겨준다.
        amountChargingHistoryRepository.save(amountChargingHistory); //repository에 해당 값을 저장하도록 시킨다.
    }
    public void plusPass(Member member, PassType passType) { // 사용자의 토큰 값을 받아 충전된 패스 시간을 기록하는 메서드
        AmountChargingHistory amountChargingHistory = new AmountChargingHistory.PassBuilder(member, passType.getMin(), passType.getPrice()).build();
        // PassType(enum)을 입력 받아 enum value에 해당하는 분(getMin), 가격(getPrice)을 빌더에게 넘겨준다.

        amountChargingHistoryRepository.save(amountChargingHistory); // repository에게 해당 값을 저장하도록 시킨다.
    }
    public void chargePriceByAdmin(Member member, Double price) { // 관리자가 직접 회원의 계정에 금액을 추가해 주는 메서드
        AmountChargingHistory amountChargingHistory = new AmountChargingHistory.PriceBuilder(member, price).build();
        // 입력 받은 충전 금액과 회원 정보를 빌더에게 넘겨준다. 이 때, 마일리지를 따로 입력하지 않았기 때문에 마일리지를 소비하지 않도록 하는 빌더에게 전달된다.
        amountChargingHistoryRepository.save(amountChargingHistory); // 이 값을 repository에게 저장하도록 시킨다.
    }
    public void chargePassByAdmin(Member member, PassType passType) { // 관리자가 직접 회원의 계정에 패스 시간을 추가해 주는 메서드
        AmountChargingHistory amountChargingHistory = new AmountChargingHistory.PassBuilder(member, passType.getMin()).build();
        // 입력 받은 패스 시간과 회원 정보를 빌더에게 넘겨준다.
        amountChargingHistoryRepository.save(amountChargingHistory); // 이 값을 repository에게 저장하도록 시킨다.
    }

    public ListResult<AmountChargingHistoryItem> getOwnChargingHistory(Member member, int page) { // 토큰 값으로 자신의 충전 내역을 가져오는 메서드.
        Page<AmountChargingHistory> originList = amountChargingHistoryRepository.findAllByMember(member, ListConvertService.getPageable(page));
        // 토큰 값 안에 담긴 회원의 데이터를 기반으로 AmountChargingHistory 테이블에서 해당하는 데이터들을 모두 가져온다. 이 값들이 많아서 한 페이지에
        // 다 보이지 않을 수도 있으니 페이지 형식으로 만들어준다.
        List<AmountChargingHistoryItem> result = new LinkedList<>();
        // 사용자에게 보여줄 필드만 따로 뽑아서 Item으로 만들고 originList에서 하나씩 뽑아서 틀에 맞게 엮어줄 예정이다.
        originList.getContent().forEach(e -> {
            AmountChargingHistoryItem item = new AmountChargingHistoryItem.Builder(e).build();
            // originList의 값(e)를 Item의 빌더에 넣어 주고,
            result.add(item); // 한 묶음으로 엮는다. 이 작업을 originList 안에 값이 다 떨어질 때까지 반복한다.
        });
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    } // 페이지 형식으로 만들었으니 반환해줘야 할 값은, 묶어 놓은 result가 되고,
    // 총 몇 개의 결과가 나왔는지 보여줘야하기 때문에 originList의 총 갯수,
    // 총 몇 페이지가 나왔는지 보여줘야 하기 때문에 originList의 총 페이지(기본 값이 10이기 때문에 총 갯수 / 10)
    // 현재 몇 페이지인지 보여줘야 하기 때문에 originList의 입력 받은 페이지 값.

    public ListResult<AmountChargingHistoryAdminItem> searchHistoryList(int page) { // 모든 충전 내역을 보여주는 메서드. 흐름은 위와 동일하다.
        Page<AmountChargingHistory> originList = amountChargingHistoryRepository.findAll(ListConvertService.getPageable(page));

        List<AmountChargingHistoryAdminItem> result = new LinkedList<>();

        originList.getContent().forEach(e -> {
            AmountChargingHistoryAdminItem item = new AmountChargingHistoryAdminItem.AdminBuilder(e).build();

            result.add(item);
        });
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    public ListResult<AmountChargingHistoryAdminItem> searchHistoryByUserName(String userName, ChargeType chargeType, int page) {
        // 아이디와 충전 타입을 받아서 해당되는 충전내역을 보여주는 메서드

        List<AmountChargingHistoryAdminItem> result = new LinkedList<>();
        Page<AmountChargingHistory> originList;

        // 충전 타입은 필수가 아니기 때문에
        if (chargeType == null) { //만약 충전 타입을 선택하지 않으면 아이디에 해당하는 모든 값들을,
            originList = amountChargingHistoryRepository.findAllByMember_Username(userName, ListConvertService.getPageable(page));
        } else { // 만약 충전 타입을 선택했다면, 아이디와 충전타입에 해당하는 모든 값들을 가져온다.
            originList = amountChargingHistoryRepository.findAllByMember_UsernameAndChargeType(userName, chargeType, ListConvertService.getPageable(page));
        }

        // 나머지 흐름은 위와 동일하다.
        originList.getContent().forEach(e -> {
            AmountChargingHistoryAdminItem item = new AmountChargingHistoryAdminItem.AdminBuilder(e).build();
            result.add(item);
        });
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }
    public ListResult<AmountChargeHistorySaleStatsItem> getSaleStats(LocalDate startDate, LocalDate endDate, int page){
        // 날짜 2개를 받아 사이의 결제내역을 보여주는 메서드
        LocalDateTime start = LocalDateTime.of(startDate, LocalTime.of(0,0,0));
        // 시작일을 받는데, 사용자가 시 분 초까지 다 입력하진 않으니, 월 일을 입력받고, 시 분 초를 각각 0으로 초기화 해준다.
        LocalDateTime end = LocalDateTime.of(endDate, LocalTime.of(23,59,59));
        // 종료일도 마찬가지로 종료일의 초기값은 입력받는 날이 끝나기 전까지로 초기화 해준다.

        Page<AmountChargingHistory> originList = amountChargingHistoryRepository.findAllByDateRegBetweenAndChargeTypeNot(start, end, ChargeType.MILEAGE, ListConvertService.getPageable(page));
        // 복수R의 흐름과 동일하다.
        List<AmountChargeHistorySaleStatsItem> result = new LinkedList<>();

        originList.getContent().forEach(e->{
            AmountChargeHistorySaleStatsItem item = new AmountChargeHistorySaleStatsItem.Builder(e).build();

            result.add(item);
        });
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }
}
