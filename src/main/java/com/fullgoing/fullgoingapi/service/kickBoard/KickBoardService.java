package com.fullgoing.fullgoingapi.service.kickBoard;

import com.fullgoing.fullgoingapi.entity.KickBoard;
import com.fullgoing.fullgoingapi.enums.KickBoardStatus;
import com.fullgoing.fullgoingapi.enums.PriceBasis;
import com.fullgoing.fullgoingapi.exception.CExistSameModelNameException;
import com.fullgoing.fullgoingapi.exception.CMissingDataException;
import com.fullgoing.fullgoingapi.lib.CommonFile;
import com.fullgoing.fullgoingapi.model.common.ListResult;
import com.fullgoing.fullgoingapi.model.kickboard.*;
import com.fullgoing.fullgoingapi.repository.KickBoardRepository;
import com.fullgoing.fullgoingapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class KickBoardService { // 킥보드 관리 서비스
    private final KickBoardRepository kickBoardRepository;

    @PersistenceContext
    EntityManager entityManager; // SQL 쿼리용


    public void setKickBoard(KickBoardRequest kickBoard){ // 킥보드 단일 등록 메서드
        Optional<KickBoard> check = kickBoardRepository.findByModelName(kickBoard.getModelName());
        // modelName은 uniqe 처리 되어 있기 때문에 테이블로 동록되어 에러가 뜨기 전에 사전에 확인 해준다.

        if (check.isPresent()) throw new CExistSameModelNameException();
        // 만약 동일한 modelName이 존재한다면 exception처리.

        KickBoard setData = new KickBoard.Builder(kickBoard).build();
        // 없다면 해당 데이터를 킥보드 테이블의 빌더에게 전달하고

        kickBoardRepository.save(setData);
        // repository에게 저장하도록 시킨다.
    }
    public void setKickBoards(MultipartFile csvFile) throws IOException { // 킥보드 csv파일로 대량 등록
        File file = CommonFile.multipartToFile(csvFile); // 나눠진 파일을 하나의 파일로 합쳐서 가져오고,
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file)); // 버퍼 리터에 파일을 올려서 한 줄씩 받아올 예정이다.

        String line = ""; // 한 줄씩 나오는 파일의 데이터를 담을 변수이고,
        int index = 0; // 줄 번호
        while ((line = bufferedReader.readLine()) != null) { // 버퍼리더에 읽어올 데이터가 더 없을 때까지 반복시킨다.
            if (index > 0) { // 0 번째 줄에 들어간 정보는 대부분 각 필드의 이름이기 때문에 제외하고 넣기 때문에 0보다 커야 입력되도록 한다.
                String[] cols = line.split(","); // 쉼표를 기준으로 나누고,

                // 킥보드 등록하려면 빌더 호출해야하는데 빌더에서 리퀘스트를 필요로 하기 때문에 준비한다.
                KickBoardsRequest kickBoardsRequest = new KickBoardsRequest(); // 리퀘스트의 생성자를 만들고
                kickBoardsRequest.setModelName(cols[0]); // 나눠진 0 번째 순서에는 modelName이 들어가게 된다.
                kickBoardsRequest.setPriceBasis(PriceBasis.valueOf(cols[1])); // 나눠진 1 번째 순서에는 가격기준코드가 들어가게 된다.
                kickBoardsRequest.setPosX(Double.parseDouble(cols[2])); // 나눠진 2 번째 순서에는 위도가 들어가게 된다
                kickBoardsRequest.setPosY(Double.parseDouble(cols[3])); // 나눠진 3 번째 순서에는 경도가 들어가게 된다.
                kickBoardsRequest.setDateBuy(LocalDate.now()); // 구매 날째는 등록일 기준
                kickBoardsRequest.setKickBoardStatus(KickBoardStatus.READY); // 킥보드 상태는 대기로 넣어준다.

                KickBoard kickBoard = new KickBoard.csvBuilder(kickBoardsRequest).build(); // 준비한 리퀘스트 주면서 킥보드로 바꿔오고
                kickBoardRepository.save(kickBoard); // repository에게 저장시킨다.
            }
            index++; // index = index + 1;
        }
        bufferedReader.close(); // 끝나면 버퍼리더를 닫아준다.
    }

    public ListResult<KickBoardItem> getNearList(double positionx, double positiony, int distanceKm) { // 근처 킥보드 가져오는 메서드
        double distance = distanceKm * 1000; // 사람들이 요청할때 근처 1키로미터 ~~ 이렇게 요청하면 이걸 미터 기준으로 변환.. x 1000

        String queryString = "select * from public.get_near_kickboard(" + positionx + ", " + positiony + ", " + distance + ")"; // 쿼리 문자열(String)로 준비하기
        Query nativeQuery = entityManager.createNativeQuery(queryString); // entityManager(DB 담당하는애)한테 위에서 작성한 쿼리모양 문자열을 쿼리 객체로 바꿔주기
        List<Object[]> queryList = nativeQuery.getResultList(); // 쿼리 실행해서 리스트 가져오기.. 근데 얘는 모양이 확정된애가 아니라서 object(타입이 아무것도 정해지지않은 객체)로 가져오기

        List<KickBoardItem> result = new LinkedList<>(); // 결과값 담을 빈 리스트 준비
        for (Object[] queryItem : queryList) { // 쿼리 실행해서 가져온 리스트에서 한줄씩 던져주면서
            result.add( // 결과값 담을 리스트에 추가해준다.
                    new KickBoardItem.Builder( // KickBoardItem 모양으로 담아야 하니까 KickBoardItem 빌더 호출
                            KickBoardStatus.valueOf(queryItem[0].toString()), // KickBoardStatus Enum 값들중에서 queryItem의 0번째 요소를 문자열로 바꾼애랑 일치하는 값이 있다면 그 값으로 바꿔오기
                            queryItem[1].toString(), // queryItem의 1번째 요소를 문자열로 캐스팅
                            PriceBasis.valueOf(queryItem[2].toString()), // PriceBasis Enum 값들중에서 queryItem의 2번째 요소를 문자열로 바꾼애랑 일치하는 값이 있다면 그 값으로 바꿔오기
                            Double.parseDouble(queryItem[3].toString()), // queryItem의 3번째 요소를 오브젝트 -> 문자열 -> 더블로 2번 캐스팅
                            Double.parseDouble(queryItem[4].toString()), // queryItem의 4번째 요소를 오브젝트 -> 문자열 -> 더블로 2번 캐스팅
                            (int)Math.round(Double.parseDouble(queryItem[5].toString())) // queryItem의 5번째 요소를 오브젝트 -> 문자열 -> 더블로 2번 캐스팅
                    ).build()
            );
        }

        return ListConvertService.settingResult(result); // 결과값이 다 완성되었으므로 return 시키기
    }
    public ListResult<KickBoardPageItem> getKickBoardListByPage(int page){ // 킥보드 리스트 가져오는 메서드
        Page<KickBoard> originList = kickBoardRepository.findAllByOrderByIdAsc(ListConvertService.getPageable(page));
        // 볼 게시글에 해당하는 모든 데이터를 commentsRepository에게 찾아오도록 시킨다.
        // 이 값들이 많아서 한 페이지에 다 보이지 않을 수도 있으니 페이지 형식으로 만들어준다.

        List<KickBoardPageItem> result = new LinkedList<>();
        // 사용자에게 보여줄 필드만 따로 뽑아서 Item으로 만들고 originList에서 하나씩 뽑아서 틀에 맞게 엮어줄 예정이다.

        originList.getContent().forEach(e->{
            KickBoardPageItem item = new KickBoardPageItem.Builder(e).build(); // originList의 값(e)를 Item의 빌더에 넣어 주고,
            result.add(item); // 한 묶음으로 엮는다. 이 작업을 originList 안에 값이 다 떨어질 때까지 반복한다.
        });
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }// 페이지 형식으로 만들었으니 반환해줘야 할 값은, 묶어 놓은 result가 되고,
    // 총 몇 개의 결과가 나왔는지 보여줘야하기 때문에 originList의 총 갯수,
    // 총 몇 페이지가 나왔는지 보여줘야 하기 때문에 originList의 총 페이지(기본 값이 10이기 때문에 총 갯수 / 10)
    // 현재 몇 페이지인지 보여줘야 하기 때문에 originList의 입력 받은 페이지 값.

    public KickBoardResponse getKickBoardSingleData(long id){ // 킥보드 정보 상세보기
        KickBoard kickBoard = kickBoardRepository.findById(id).orElseThrow(CMissingDataException::new);
        // 킥보드 id를 repository에게 찾아오라고 시키되, 없으면 exception 처리 한다.

        return new KickBoardResponse.Builder(kickBoard).build();
        // 찾아온 데이터를 보여줄 그릇인 KickBoardResponse의 빌더의 틀에 맞춰 넣어주고 return 시킨다.
    }

    public void putNamePricePosXYMemoIsUse(long id, KickBoardStatus kickBoardStatus, KickBoardNamePriceXYMemoIsUseUpdateRequest updateRequest){
        // 킥보드의 정보를 변경하는 메서드
        KickBoard kickBoard = kickBoardRepository.findById(id).orElseThrow(CMissingDataException::new);
        // 킥보드 id를 repository에게 찾아오라고 시키되, 없으면 exception 처리 한다.

        kickBoard.putNamePricePosXYMemoIsUse(kickBoardStatus, updateRequest);
        // 찾아온 데이터의 값들을 받아온 kickBoardStatus와 updateRequest의 값에 맞게 수정하는 KickBoard의 메서드에 넣어준다.

        kickBoardRepository.save(kickBoard);
        // repository에게 저장하도록 시킨다.
    }

    public KickBoard getData(long id){ // 킥보드 원본 가져오기
        return kickBoardRepository.findById(id).orElseThrow(CMissingDataException::new);
        // 킥보드 id를 repository에게 찾아오라고 시키고 return 시킨다. 없으면 exception 처리 한다.
    }
}
