package com.fullgoing.fullgoingapi.service.member;

import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.enums.MemberGroup;
import com.fullgoing.fullgoingapi.enums.MessageTemplate;
import com.fullgoing.fullgoingapi.exception.*;
import com.fullgoing.fullgoingapi.lib.CommonCheck;
import com.fullgoing.fullgoingapi.model.member.*;
import com.fullgoing.fullgoingapi.repository.MemberRepository;
import com.fullgoing.fullgoingapi.service.authCheck.AuthCheckService;
import com.fullgoing.fullgoingapi.service.messageSendHistory.MessageSendHistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;
    private final AuthCheckService authCheckService;
    private final MessageSendHistoryService messageSendHistoryService;
    private final PasswordEncoder passwordEncoder;

    public void setFirstMember() {
        String username = "superadmin";
        String password = "idjh195233";
        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            MemberJoinRequest joinRequest = new MemberJoinRequest();
            joinRequest.setUsername(username);
            joinRequest.setPassword(password);
            joinRequest.setPasswordRe(password);
            joinRequest.setName("최고관리자");
            joinRequest.setPhoneNumber("010-1111-1111");

            setAdmin(MemberGroup.ROLE_ADMIN, joinRequest);
        }
    }
    public Member setMember(MemberGroup memberGroup, MemberCreateRequest createRequest) {
        if (!CommonCheck.checkUsername(createRequest.getUsername())) throw new CUnValidTypeOfUsernameException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!createRequest.getPassword().equals(createRequest.getPasswordRe())) throw new CPutNotSameRePasswordException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(createRequest.getUsername())) throw new CExistSameUsernameException(); // 중복된 아이디가 존재합니다 던지기
        if (!CommonCheck.checkPhone(createRequest.getPhoneNumber())) throw new CWrongPhoneNumberException(); //유효한 핸드폰 번호 형식이 아닙니다 던지기

        if (!isNewPhoneNum(createRequest.getPhoneNumber())) throw new CExistSamePhoneNumException(); //동일한 휴대폰 번호가 이미 등록되어 있는지 확인한다.

        if (!authCheckService.checkAuthStatus(createRequest.getPhoneNumber())) throw new CMissingAuthSuccessException(); //인증이 완료되지 않았습니다 던지기

        createRequest.setPassword(passwordEncoder.encode(createRequest.getPassword()));

        Member member = new Member.MemberBuilder(memberGroup, createRequest).build();
        authCheckService.deleteCheckNum(createRequest.getPhoneNumber());
        return memberRepository.save(member);
    }
    public Member setAdmin(MemberGroup memberGroup, MemberJoinRequest memberJoinRequest) {
        if (!CommonCheck.checkUsername(memberJoinRequest.getUsername())) throw new CUnValidTypeOfUsernameException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!memberJoinRequest.getPassword().equals(memberJoinRequest.getPasswordRe())) throw new CPutNotSameRePasswordException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(memberJoinRequest.getUsername())) throw new CExistSameUsernameException(); // 중복된 아이디가 존재합니다 던지기
        if (!CommonCheck.checkPhone(memberJoinRequest.getPhoneNumber())) throw new CWrongPhoneNumberException(); //유효한 핸드폰 번호 형식이 아닙니다 던지기
        if (!isNewPhoneNum(memberJoinRequest.getPhoneNumber())) throw new CExistSamePhoneNumException(); //동일한 휴대폰 번호가 이미 등록되어 있는지 확인한다.

        if (memberJoinRequest.getLicenceNumber() == null || memberJoinRequest.getLicenceNumber().isEmpty()) {
            memberJoinRequest.setLicenceNumber("");
        } else {
            if (!CommonCheck.checkLicence(memberJoinRequest.getLicenceNumber())) throw new CWrongLicenceNumberException();
            if (!isNewLicenceNum(memberJoinRequest.getLicenceNumber())) throw new CExistSameLicenceNumberException();
        }

        memberJoinRequest.setPassword(passwordEncoder.encode(memberJoinRequest.getPassword()));

        Member member = new Member.AdminBuilder(memberGroup, memberJoinRequest).build();
        return memberRepository.save(member);
    }

    public void putPhoneNumPassword(Member member, MemberPasswordPhoneNumUpdateRequest memberPasswordPhoneNumUpdateRequest){
        if (!passwordEncoder.matches(memberPasswordPhoneNumUpdateRequest.getCurrentPassword(), member.getPassword())  ) throw new CPutWrongCurrentPasswordException();
        if (!memberPasswordPhoneNumUpdateRequest.getNewPassword().equals(memberPasswordPhoneNumUpdateRequest.getNewRePassword())) throw new CPutNotSameRePasswordException();

        memberPasswordPhoneNumUpdateRequest.setNewPassword(passwordEncoder.encode(memberPasswordPhoneNumUpdateRequest.getNewPassword()));
        member.putPhoneNumPassword(memberPasswordPhoneNumUpdateRequest);
        memberRepository.save(member);
    }

    public void findPassword(FindPasswordRequest findPasswordRequest){
        Optional<Member> member = memberRepository.findByUsernameAndPhoneNumber(findPasswordRequest.getUsername(), findPasswordRequest.getPhoneNumber());

        if (member.isEmpty()) throw new CMissingAuthSuccessException();

        String tempPassword = authCheckService.makeRandomNum(8);
        Member memberData = member.get();
        memberData.putPassword(passwordEncoder.encode(tempPassword));
        memberRepository.save(memberData);

        HashMap<String, String> msgMap = new HashMap<>();
        msgMap.put("비밀번호", tempPassword);
        messageSendHistoryService.sendMessage(MessageTemplate.TP_1234,"010-1111-1111", findPasswordRequest.getPhoneNumber(), msgMap);
    }

    public void setLicenceNum(Member member, MemberLicenceNumUpdateRequest updateRequest){
        if (!CommonCheck.checkLicence(updateRequest.getLicenceNumber())) throw new CWrongLicenceNumberException();
        if (!isNewLicenceNum(updateRequest.getLicenceNumber())) throw new CExistSameLicenceNumberException();

        member.setLicenceNumber(updateRequest);
        memberRepository.save(member);
    }

    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;
    }

    private boolean isNewPhoneNum(String phoneNum){
        long dupPhone = memberRepository.countByPhoneNumber(phoneNum);
        return dupPhone <= 0;
    }

    private boolean isNewLicenceNum(String licenceNum){
        long dupLicence = memberRepository.countByLicenceNumber(licenceNum);
        return dupLicence <= 0;
    }

    public void phoneNumCheckForAuth(String phoneNum){
        long dupPhone = memberRepository.countByPhoneNumber(phoneNum);
        if (dupPhone > 0) throw new CExistSamePhoneNumException();
    }

    public Member getMemberById(long memberId){
        return memberRepository.findById(memberId).orElseThrow(CMissingAuthSuccessException::new);
    }

}
