package com.fullgoing.fullgoingapi.service.member;

import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.entity.RemainingAmount;
import com.fullgoing.fullgoingapi.model.common.ListResult;
import com.fullgoing.fullgoingapi.model.member.MemberInfoItem;
import com.fullgoing.fullgoingapi.model.member.MemberInfoResponse;
import com.fullgoing.fullgoingapi.repository.RemainingAmountRepository;
import com.fullgoing.fullgoingapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberInfoService {
    private final RemainingAmountRepository remainingAmountRepository;

    public MemberInfoResponse getMemberData(Member member){
        RemainingAmount originData = remainingAmountRepository.findByMember(member).orElseThrow();

        return new MemberInfoResponse.Builder(member, originData).build();
    }
    public ListResult<MemberInfoItem> getMemberListByPage(int page){
        Page<RemainingAmount> originList = remainingAmountRepository.findAllByOrderByIdAsc(ListConvertService.getPageable(page));

        List<MemberInfoItem> result = new LinkedList<>();

        originList.getContent().forEach(e->{
            MemberInfoItem memberInfoItem = new MemberInfoItem.Builder(e).build();
            result.add(memberInfoItem);
        });
        return ListConvertService.settingResult(result,originList.getTotalElements(),originList.getTotalPages(),originList.getPageable().getPageNumber());
    }

}
