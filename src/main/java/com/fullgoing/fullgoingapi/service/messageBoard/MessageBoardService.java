package com.fullgoing.fullgoingapi.service.messageBoard;

import com.fullgoing.fullgoingapi.entity.MessageBoard;
import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.enums.MemberGroup;
import com.fullgoing.fullgoingapi.exception.CMissingDataException;
import com.fullgoing.fullgoingapi.exception.CNoPermissionDeleteContentException;
import com.fullgoing.fullgoingapi.exception.CNoPermissionPutContentException;
import com.fullgoing.fullgoingapi.model.messageBoard.MessageBoardItem;
import com.fullgoing.fullgoingapi.model.messageBoard.MessageBoardRequest;
import com.fullgoing.fullgoingapi.model.messageBoard.MessageBoardResponse;
import com.fullgoing.fullgoingapi.model.messageBoard.MessageBoardTitleNameImgUpdateRequest;
import com.fullgoing.fullgoingapi.model.common.ListResult;
import com.fullgoing.fullgoingapi.repository.MessageBoardRepository;
import com.fullgoing.fullgoingapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MessageBoardService {
    private final MessageBoardRepository messageBoardRepository;

    public void setBoardHistory(MessageBoardRequest messageBoardRequest){
        MessageBoard messageBoard = new MessageBoard.Builder(messageBoardRequest).build();

        messageBoardRepository.save(messageBoard);
    }
    public ListResult<MessageBoardItem> getBoardHistoriesByPage(int page){
        Page<MessageBoard> originList = messageBoardRepository.findAllByOrderByIdDesc(ListConvertService.getPageable(page));

        List<MessageBoardItem> result = new LinkedList<>();

        originList.getContent().forEach(e->{
            MessageBoardItem messageBoardItem = new MessageBoardItem.Builder(e).build();

            result.add(messageBoardItem);
        });
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    //    public ListResult<BoardHistoryItem> getBoardHistories() {
//        List<BoardHistory> originList = boardHistoryRepository.findAllByOrderByWroteDateDesc();
//
//        List<BoardHistoryItem> result = new LinkedList<>();
//
//        originList.forEach(e -> {
//            BoardHistoryItem boardHistoryItem = new BoardHistoryItem.SameDateBuilder(e).build();
//
//            result.add(boardHistoryItem);
//        });
//        return ListConvertService.settingResult(result);
//    } // 페이징 없는 일반 리스트
    public MessageBoardResponse getBoardHistory(long id){
        MessageBoard messageBoard = messageBoardRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new MessageBoardResponse.Builder(messageBoard).build();
    }

    public void putTitleThemeContext(long id, MessageBoardTitleNameImgUpdateRequest updateRequest){
        MessageBoard messageBoard = messageBoardRepository.findById(id).orElseThrow(CMissingDataException::new);
        messageBoard.putTitleThemeContext(updateRequest);

        messageBoardRepository.save(messageBoard);
    }
    public void deleteContent(long id){
        messageBoardRepository.deleteById(id);
    }


    public MessageBoard getBoardHistoryData(long id){
        return messageBoardRepository.findById(id).orElseThrow(CMissingDataException::new);
    }
}
