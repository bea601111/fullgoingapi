package com.fullgoing.fullgoingapi.service.messageSendHistory;

import com.fullgoing.fullgoingapi.entity.MessageSendHistory;
import com.fullgoing.fullgoingapi.enums.MessageTemplate;
import com.fullgoing.fullgoingapi.repository.MessageSendHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
@RequiredArgsConstructor

public class MessageSendHistoryService {
    private final MessageSendHistoryRepository messageSendHistoryRepository;

    public MessageSendHistory sendMessage(MessageTemplate messageTemplate, String sendNum, String receiveNum, HashMap<String, String> contents){
        //알리고 api 호출
        //보낸 내역 저장하기
        String messageContentsOrigin = messageTemplate.getContents();
        for (String key : contents.keySet()) {
            String keyResult = "#\\{" + key + "}";
            messageContentsOrigin = messageContentsOrigin.replaceAll(keyResult, contents.get(key));
        }

        MessageSendHistory messageSendHistory = new MessageSendHistory.Builder(sendNum, receiveNum, messageContentsOrigin).build();
        return messageSendHistoryRepository.save(messageSendHistory);
    }
}
