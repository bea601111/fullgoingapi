package com.fullgoing.fullgoingapi.service.remainingAmount;

import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.entity.RemainingAmount;
import com.fullgoing.fullgoingapi.enums.PassType;
import com.fullgoing.fullgoingapi.enums.PriceBasis;
import com.fullgoing.fullgoingapi.exception.CLessThanChargeMinPriceException;
import com.fullgoing.fullgoingapi.exception.CMinusRemainMoneyException;
import com.fullgoing.fullgoingapi.exception.CMissingDataException;
import com.fullgoing.fullgoingapi.exception.CMoreThanChargeMaxPriceException;
import com.fullgoing.fullgoingapi.model.remainingAmount.RemainingAmountChargePriceRequest;
import com.fullgoing.fullgoingapi.repository.RemainingAmountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor

public class RemainingAmountService {
    private final RemainingAmountRepository remainingAmountRepository;

    public void setRemainingAmount(Member member){
        RemainingAmount remainingAmount = new RemainingAmount.Builder(member).build();

        remainingAmountRepository.save(remainingAmount);
    }
    public RemainingAmount getDataByMember(Member member){
        return remainingAmountRepository.findByMember(member).orElseThrow(CMissingDataException::new);
    }
    public void checkRemainMoney(Member member, double basePrice){
        RemainingAmount remainingAmount = remainingAmountRepository.findByMember(member).orElseThrow(CMissingDataException::new);

        if (remainingAmount.getRemainPrice() <= basePrice) throw new CMinusRemainMoneyException();
    }
    public double putRemainPricePass(Member member, Double price, Double pass){
        RemainingAmount remainingAmount = remainingAmountRepository.findByMember(member).orElseThrow(CMissingDataException::new);

        double mileage;
        if (remainingAmount.getRemainPrice() <= price){
            mileage = remainingAmount.getRemainPrice() * 0.01;
        } else mileage = price * 0.01;
        remainingAmount.putRemainPricePass(price,pass,mileage);

         remainingAmountRepository.save(remainingAmount);

         return mileage;
    }
    public void chargePrice(Member member, RemainingAmountChargePriceRequest request){
        RemainingAmount remainingAmount = remainingAmountRepository.findByMember(member).orElseThrow(CMissingDataException::new);

        double price = request.getPrice();
        double mileage = remainingAmount.getRemainMileage();

        if (price >= mileage){
            mileage = 0D;
        } else {
            mileage -= price;
        }

        remainingAmount.putRemainPriceMileage(price,mileage);

        remainingAmountRepository.save(remainingAmount);
    }

    public void chargePass(Member member, PassType passType){
        RemainingAmount remainingAmount = remainingAmountRepository.findByMember(member).orElseThrow(CMissingDataException::new);
        remainingAmount.putPlusPass(passType.getMin());

        remainingAmountRepository.save(remainingAmount);
    }

    public void chargePriceByAdmin(Member member, RemainingAmountChargePriceRequest request){
        RemainingAmount remainingAmount = remainingAmountRepository.findByMember(member).orElseThrow(CMissingDataException::new);

        remainingAmount.putRemainPriceByAdmin(request.getPrice());

        remainingAmountRepository.save(remainingAmount);
    }
    public void chargePassByAdmin(Member member, PassType passType){
        RemainingAmount remainingAmount = remainingAmountRepository.findByMember(member).orElseThrow(CMissingDataException::new);

        remainingAmount.putPlusPass(passType.getMin());

        remainingAmountRepository.save(remainingAmount);
    }

}
