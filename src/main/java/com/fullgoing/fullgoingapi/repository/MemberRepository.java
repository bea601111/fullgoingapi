package com.fullgoing.fullgoingapi.repository;

import com.fullgoing.fullgoingapi.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Optional<Member> findByUsername(String username);
    // 로그인, 프로필 등에서 아이디를 찾아서 테이블에 존재하지 않으면 exception처리 하기용.

    Page<Member> findAllByOrderByIdAsc(Pageable pageable);
    // 모든 회원 가져오기용.

    long countByUsername(String username);
    // 아이디 중복체크용.

    long countByPhoneNumber(String phoneNum);
    // 전화번호 중복체크용.

    long countByLicenceNumber(String licenceNum);
    // 면허번호 중복체크용.

    Optional<Member> findByUsernameAndPhoneNumber(String userName, String phoneNumber);
    // 테이블에서 아이디와 전화번호를 입력받아 일치하는 정보가 있으면 해당 전화번호로 임시비밀번호 발송용.
}
