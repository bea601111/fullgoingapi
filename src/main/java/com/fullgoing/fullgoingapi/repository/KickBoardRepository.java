package com.fullgoing.fullgoingapi.repository;

import com.fullgoing.fullgoingapi.entity.KickBoard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface KickBoardRepository extends JpaRepository<KickBoard, Long> {
    Page<KickBoard> findAllByOrderByIdAsc(Pageable pageable);
    // 모든 킥보드 보여주기용.

    Optional<KickBoard> findByModelName(String modelName);
    // modelName 필드는 unique 타입이기 때문에 중복되는 modelName Exception처리용.
}
