package com.fullgoing.fullgoingapi.repository;

import com.fullgoing.fullgoingapi.entity.AmountChargingHistory;
import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.enums.ChargeType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface AmountChargingHistoryRepository extends JpaRepository<AmountChargingHistory, Long> {


    Page<AmountChargingHistory> findAllByMember(Member member, Pageable pageable);
    // 자신의 충전 내역 찾기용

    Page<AmountChargingHistory> findAllByMember_Username(String userName, Pageable pageable);
    // 아이디로 충전 내역 검색용

    Page<AmountChargingHistory> findAllByMember_UsernameAndChargeType(String userName, ChargeType chargeType, Pageable pageable);
    // 아이디와 충전 타입으로 충전 내역 검색용

    Page<AmountChargingHistory> findAllByDateRegBetweenAndChargeTypeNot(LocalDateTime startDate, LocalDateTime endDate, ChargeType chargeType, Pageable pageable);
    // 마일리지 충전내역을 제외하고 두 날짜 사이의 수익 통계용
}
