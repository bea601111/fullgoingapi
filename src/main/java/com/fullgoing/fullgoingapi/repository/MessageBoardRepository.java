package com.fullgoing.fullgoingapi.repository;

import com.fullgoing.fullgoingapi.entity.MessageBoard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageBoardRepository extends JpaRepository<MessageBoard, Long> {

    Page<MessageBoard> findAllByOrderByIdDesc(Pageable pageable);
    // 게시판에 최근에 등록된 글부터 보여주기용.

}
