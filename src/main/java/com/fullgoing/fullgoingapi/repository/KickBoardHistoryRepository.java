package com.fullgoing.fullgoingapi.repository;

import com.fullgoing.fullgoingapi.entity.KickBoardHistory;
import com.fullgoing.fullgoingapi.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

public interface KickBoardHistoryRepository extends JpaRepository<KickBoardHistory, Long> {
    Optional<KickBoardHistory> findByMemberAndIsComplete(Member member, Boolean isComplete);
    // 테이블에서 회원의 사용 중 여부를 찾아 킥보드 중복시작 방지용.
    // 테이블에서 회원의 현재 사용 중인 이용내역을 찾아 보여주기용.

    Optional<KickBoardHistory> findByMemberAndId(Member member, Long historyId);
    // 회원의 이용내역 상세보기용.

    Page<KickBoardHistory> findAllByMemberOrderByIdDesc(Member member, Pageable pageable);
    // 회원의 이용내역 모두 보여주기용.
}
