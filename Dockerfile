# openjdk:17 버전
FROM openjdk:17

# 폴더 생성 /build
RUN mkdir /build

# 빌드된 .jar 파일 전체를 /build/app.jar 복사
ADD build/libs/*.jar /build/app.jar

# 포트 번호 8080
EXPOSE 8080

# 컨테이너가 수행하게 될 실행 명령을 정의하는 선언문
# java .jar 실행 명령문 java -jar
ENTRYPOINT ["java", "-jar", "/build/app.jar"]